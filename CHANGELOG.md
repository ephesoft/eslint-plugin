# CHANGELOG

## 2.16.0 (2022-10-21)
- Removed internal package references

## 2.15.0 (2022-10-14)
- Updated npm references

## 2.14.0 (2022-10-04)
- Updated npm references
- Updated to template version 6.0.2

## 2.13.0 (2022-09-26)
- Updated build to be more inline with other packages

## 2.12.0 (2022-09-26)
- Updated to template version 5.2.0
- Updated npm references
- Fixed npm audit warning in bitbucket-pipelines.yml

## 2.11.0 (2021-11-24)
- Updated npm references
- Updated based on sdk template 2.5.0 (these are very different types of projects)

## 2.10.0 (2021-10-27)
- Updated npm references for compatibility with latest eslint

## 2.9.0 (2021-10-25)
- Updated npm references

## 2.8.1 (2021-07-26)
- Updated npm references
- Fixed audit issues

## 2.7.1 (2021-04-28)
- Fixed latest tag (MyGet failed to reassign the "latest" tag on the last publish)

## 2.7.0 (2021-04-27)
- Updated npm references
- Updated project to use integration.test.sdk

## 2.6.0 (2020-10-01)
- Updated npm references (linter packages were updated)

## 2.5.0 (2020-09-16)
- Updated npm references (linter packages were updated)

## 2.4.0 (2020-09-08)
- Updated npm references (linter packages were updated)

## 2.3.0 (2020-09-04)
- Updated npm references

## 2.2.0 (2020-08-28)
- Removed the rule to enforce template literal expressions to be of string type (@typescript-eslint/restrict-template-expressions)

## 2.1.0 (2020-08-27)
- Removed the rules to prohibit object as a type as there really is no workable alternative

## 2.0.0 (2020-08-25)
- BREAKING: Updated npm references - there are breaking changes in typescript 4.0.2
- Added ability to publish beta package
- Added check script to prevent beta version in GA release
- Fixed location of package.test.ts and added missing tests
- Added save-exact flag to .npmrc file
- Added eslint and prettier as dev dependencies so we don't forget to update them (they are peer dependencies)
- Updated documentation
- Updated to use husky

## 1.2.0 (2020-05-16)
- Removed no-use-before-define rule

## 1.1.0 (2020-04-17)
- Updated selector for memberLike to be camelCase or PascalCase
- Added selector for enumMember to be PascalCase

## 1.0.0 (2020-04-06)
- Added files for initial release

- - - - -
* Following [Semantic Versioning](https://semver.org/)
* [Changelog Formatting](https://ephesoft.atlassian.net/wiki/spaces/ZEUS1/pages/1189347481/Changelog+Formatting)
* [Major Version Migration Notes](MIGRATION.md)
