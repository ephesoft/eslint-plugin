module.exports = {
    configs: {
        typescript: {
            parser: '@typescript-eslint/parser',
            parserOptions: {
                ecmaVersion: 2018,
                sourceType: 'module'
            },
            extends: [
                'eslint:recommended',
                'plugin:@typescript-eslint/eslint-recommended',
                'plugin:@typescript-eslint/recommended',
                'plugin:@typescript-eslint/recommended-requiring-type-checking',
                'plugin:prettier/recommended',
            ],
            // For a list of available rules, see:
            // https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin#supported-rules
            rules: {
                '@typescript-eslint/restrict-template-expressions': 'off',
                '@typescript-eslint/no-unused-vars': 'error',
                '@typescript-eslint/no-inferrable-types': 'off',
                // @typescript-eslint/no-use-before-define was disabled as per the architecture meeting on 2020-05-15
                '@typescript-eslint/no-use-before-define': 'off',
                // Make sure async functions are handled properly
                '@typescript-eslint/no-floating-promises': 'error',
                // Don't allow classes as namespaces with only static methods
                '@typescript-eslint/no-extraneous-class': 'error',
                // Check our naming conventions
                '@typescript-eslint/naming-convention': [
                    'error',
                    { selector: 'default', format: ['camelCase'] },
                    { selector: 'variable', format: ['camelCase', 'UPPER_CASE'] },
                    { selector: 'variableLike', format: ['camelCase'] },
                    { selector: 'typeLike', format: ['PascalCase'] },
                    {
                        selector: 'memberLike',
                        format: ['camelCase', 'PascalCase'],
                        leadingUnderscore: 'forbid',
                        trailingUnderscore: 'forbid'
                    },
                    { selector: 'enumMember', format: ['PascalCase'] }
                ],
                // Prevent complaints about object and {}
                "@typescript-eslint/ban-types": [
                    "error",
                    {
                        types: {
                            object: false
                        },
                        extendDefaults: true
                    }
                ]
            }
        }
    }
};
